/* queue.c: Queue Structure (Semaphore) */

#include "memhashed/queue.h"

/**
 * Create Queue Structure.
 * @param   sentinel        Sentinel used to signal end of queue processing.
 * @param   capacity        Maximum size of queue.
 * @return  Newly allocated Queue Structure.
 */
Queue *	queue_create(int64_t sentinel, size_t capacity) {
    Queue *queue = calloc(1, sizeof(Queue));
    if (queue) {
    	queue->data     = calloc(capacity, sizeof(int64_t));
    	queue->sentinel = sentinel;
    	queue->capacity = capacity;

        sem_init(&queue->lock    , 0, 1);
        sem_init(&queue->produced, 0, 0);
        sem_init(&queue->consumed, 0, queue->capacity);
    }
    return queue;
}

/**
 * Delete Queue Structure (including internal data).
 * @param   queue       Pointer to Queue Structure.
 */
void	queue_delete(Queue *queue) {
    if (queue) {
    	free(queue->data);
    	free(queue);
    }
}

/**
 * Push value to end of Queue Structure.
 * @param   queue       Pointer to Queue Structure.
 * @param   value       Value to add to end of Queue Structure.
 */
void	queue_push(Queue *queue, int64_t value) {
    sem_wait(&queue->consumed);
    sem_wait(&queue->lock);

    queue->data[queue->writer] = value;
    queue->writer	   = (queue->writer + 1) % queue->capacity;
    queue->size++;

    sem_post(&queue->lock);
    sem_post(&queue->produced);
}

/**
 * Pop value from front of Queue Structure.
 * @param   queue       Pointer to Queue Structure.
 */
int64_t	queue_pop(Queue *queue) {
    sem_wait(&queue->produced);
    sem_wait(&queue->lock);
    int64_t value = queue->data[queue->reader];
    if (value != queue->sentinel) {
        queue->reader = (queue->reader + 1) % queue->capacity;
        queue->size--;
    }
    sem_post(&queue->lock);
    if (value != queue->sentinel) {
        sem_post(&queue->consumed);
    } else {
        sem_post(&queue->produced);
    }
    return value;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
