/* func_scan.c: compute sum of array */

#include "memhashed/cache.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define N    (1<<10)

/* Globals */

int64_t Data[N] = {0};

/* Thread */

void *	scan_thread(void *arg) {
    Cache *cache = (Cache *)arg;

    int64_t total = 0;
    for (int64_t j = 0; j < 10; j++) {
	for (int64_t key = 0; key < N; key++) {
	    total += cache_get(cache, key);

	}
    }

    return (void *)total;
}

/* Handler */

int64_t	scan_handler(const uint64_t key) {
    return Data[key];
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    size_t addrlen   = strtol(argv[1], NULL, 10);
    size_t page_size = strtol(argv[2], NULL, 10);
    Policy policy    = strtol(argv[3], NULL, 10);
    size_t nthreads  = strtol(argv[4], NULL, 10);

    for (size_t i = 0; i < N; i++) {
    	Data[i] = i % 256;
    }

    Cache *cache = cache_create(addrlen, page_size, policy, scan_handler);
    assert(cache);

    Thread threads[nthreads];
    for (size_t t = 0; t < nthreads; t++) {
    	thread_create(&threads[t], NULL, scan_thread, cache);
    }

    for (size_t t = 0; t < nthreads; t++) {
    	size_t result;
    	thread_join(threads[t], (void **)&result);
    	assert(result == 1305600);
    }

    cache_stats(cache, stdout);
    cache_delete(cache);

    return EXIT_SUCCESS;
}
